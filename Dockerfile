FROM ubuntu:bionic
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
    chromium-browser \
    npm \
    pandoc \
    texlive-full \
    && apt-get clean

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true
RUN npm install --global mermaid-filter

ADD .puppeteer.json /pandoc/.puppeteer.json
WORKDIR /pandoc

VOLUME /source/
ENTRYPOINT [ "pandoc", "-F", "mermaid-filter" ]
